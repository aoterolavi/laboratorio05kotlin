package com.aotero.laboratorio05kotlin

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.dialogo.*

class MainActivity : AppCompatActivity(),IListas {
    var lenguajeElegido = ""
    var contador = 0
    var seleccionado=""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val marcas = listOf("Kia", "Daewoo","Bugatti")

        val adapter = ArrayAdapter(this, R.layout.style_spinner, marcas)

        spMarcas.adapter = adapter



        spMarcas.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                //Toast.makeText(this@MainActivity, languages[position], Toast.LENGTH_SHORT).show()

                if (contador!=0){
                 seleccionado=  marcas[position]
                }
                contador++

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // write code to perform some action
            }
        }

        btnVerificar.setOnClickListener{
            createLoginDialogo(seleccionado
                               ,edtNombres.text.toString()
                               ,edtAnoVehiculo.text.toString()
                               ,edtProblema.text.toString()).show()
        }
    }




override fun createLoginDialogo(marca: String,nombres:String,anio:String,descripcion:String): AlertDialog {
        val alertDialog: AlertDialog
        val builder  = AlertDialog.Builder(this)
        val inflater = layoutInflater
        val v: View = inflater.inflate(R.layout.dialogo, null)

        builder.setView(v)

        val tvNombres:TextView=v.findViewById(R.id.tvNombres)
        val tvMarca :TextView=v.findViewById(R.id.tvMarca)
        val tvanio:TextView=v.findViewById(R.id.tvAnio)
        val tvdescr:TextView=v.findViewById(R.id.tvdescProblema)
        val btnAperturarNo: Button = v.findViewById(R.id.btn_aperturar_no)
        val btnAperturarSi: Button = v.findViewById(R.id.btn_aperturar_si)
        val imgLogo: ImageView = v.findViewById(R.id.imgLogo)

        tvNombres.text ="Nombres: $nombres"
        tvMarca.text="Marca: $marca"
        tvanio.text="Año: $anio"
        tvdescr.text="Problema: $descripcion"

        if (marca == "Kia")
            imgLogo.setBackgroundResource(R.drawable.icon_kia)
        else if(marca =="Daewoo")
            imgLogo.setBackgroundResource(R.drawable.icon_daewoo)
        else
            imgLogo.setBackgroundResource(R.drawable.icon_bugatti)

        alertDialog = builder.create()

        btnAperturarSi.setOnClickListener {
            lenguajeElegido = marca
            Toast.makeText(this,"Has seleccionado $marca", Toast.LENGTH_SHORT).show()
            alertDialog.dismiss()
        }

        btnAperturarNo.setOnClickListener{
            alertDialog.dismiss()
        }

        return alertDialog
    }
}