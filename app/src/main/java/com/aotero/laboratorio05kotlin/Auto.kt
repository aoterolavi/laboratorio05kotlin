package com.aotero.laboratorio05kotlin

data class Auto  (
    val propietario:String,
    val marcaAuto:String,
    val anio :String,
    val descProblema:String

)