package com.aotero.laboratorio05kotlin

import androidx.appcompat.app.AlertDialog

interface IListas {
    fun createLoginDialogo(marca:String,nombres:String,anio:String,descricion:String): AlertDialog
}